FROM python:3.10
COPY ./requirements.txt /otip_lr1/requirements.txt
ARG PROXY
RUN if [ -z "$PROXY" ]; then \
        pip install --no-cache-dir --upgrade -r /otip_lr1/requirements.txt; \
    else \
        pip install --proxy "$PROXY" --no-cache-dir --upgrade -r /otip_lr1/requirements.txt; \
    fi
COPY app/ /otip_lr1/app
COPY ./certs ./certs
WORKDIR /otip_lr1/app
RUN python -m spacy download ru_core_news_sm
ENTRYPOINT ["python3", "-m", "control"]