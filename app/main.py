from elasticsearch import Elasticsearch
import os
import re
from collections import Counter
import xml.etree.ElementTree as ET
import spacy
from spacy.lang.ru.examples import sentences
from string import punctuation


client = Elasticsearch(
    "https://es01:9200",
    ca_certs="/certs/ca/ca.crt",
    basic_auth=("elastic", "changeme"),
    verify_certs=True
)

analysis_settings = {
    "analysis": {
        "filter": {  # токен фильтр для стоп-слов русского языка и наших трех слов
            "russian_stop": {
                "type": "stop",
                "stopwords": "_russian_"
            },
            "custom_stopwords": {
                "type": "stop",
                "stopwords": ["князь", "повезет", "сорок"]
            },
            "russian_stemmer": {
                "type": "stemmer",
                "language": "russian"
            }
        },
        "analyzer": {  # кастомный анализатор
            "my_custom": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": ["lowercase", "russian_stop", "custom_stopwords", "russian_stemmer"]
            }
        }
    }
}

mapping = {
    "properties": {  # маппинг для индекса настроенный на русский язык
        "title": {"type": "text", "analyzer": "my_custom"},
        "author": {"type": "text", "analyzer": "my_custom"},
        "volume": {"type": "integer"},
        "part": {"type": "integer"},
        "chapter": {"type": "integer"},
        "content": {"type": "text", "analyzer": "my_custom"},
        "file_path": {"type": "text"},
    }
}

index_name = "2020-3-05-kle"


def create():
    """
    Функция создающая индекс с заданными параметрами
    :return: index_name
    """
    try:
        if not client.indices.exists(index=index_name):
            if client.indices.create(index=index_name, settings=analysis_settings, mappings=mapping):
                return index_name
    except IndexError as e:
        print(f"{e}: Не удалось создать индекс")
        return IndexError


# create()


def add_book(title, author, year, file_path):
    """
    Функция загрузки текста книги из файла
    :return: document
    """
    try:
        if not os.path.exists(f"/input/{file_path}"):
            raise FileNotFoundError
    except FileNotFoundError as e:
        print(f"{e}: файл не найден")
        return FileNotFoundError

    with open(f"/input/{file_path}", 'r') as file:
        content = file.read()

    document = {
        "title": title,
        "author": author,
        "year_pub": int(year),
        "content": content
    }
    try:
        client.index(index=index_name, document=document)
        print("Файл успешно добавлен")
        return document
    except IndexError as e:
        print(f"{e}: не удалось добавить индекс")
        return IndexError



# add_book("Басни", "Гоголь", 1600, "../text.txt")


def add_books(catalog_path):
    """
    Функция загрузки текста книги из файла
    :return: document
    """
    try:
        if not os.path.exists(catalog_path):
            raise FileNotFoundError

    except FileNotFoundError as e:
        print(f"{e}: дериктория не найдена")
        return FileNotFoundError

    try:
        for filename in os.listdir(catalog_path):
            file_path = os.path.join(catalog_path, filename)
            if os.path.isfile(file_path):
                match = re.match(r"(.+)\s*-\s*(.+)\s*-\s*(\d{4})", filename)  # (.+) соответствует не пустой строке
                if match:
                    title = match.group(1).strip()
                    author = match.group(2).strip()
                    year = int(match.group(3))

                    with open(file_path, 'r') as file:
                        content = file.read()

                    document = {
                        "title": title,
                        "author": author,
                        "year_pub": year,
                        "content": content
                    }

                    client.index(index=index_name, document=document)
                    print("Файл успешно добавлен")
                    return document
    except IndexError as e:
        print(f"{e}: не удалось добавить индексы из каталога")
        return IndexError



# add_books("/home/user/Desktop/otip-lr1/creation")


def count_books_with_words(word):
    """
    Функция поиска слова в тексте
    :return: result
    """
    query = {
        "match": {
            "content": word
        }
    }
    try:
        result = client.search(index=index_name, query=query)
        return result
    except Exception as e:
        print(f"{e}: не удалось посчитать количество документов со словом {word}")
        return Exception

    total_books = result["hits"]["total"]["value"]
    print(f"Найдено {total_books} книг")

    for hit in result["hits"]["hits"]:
        book_title = hit["_source"]["title"]
        author = hit["_source"]["author"]
        year_pub = hit["_source"]["year_pub"]
        print(f"{book_title}, {author}, {year_pub}")


def search_books(author, word):
    """
    Функция поиска всех книг автора
    :return: result
    """
    query = {
        "bool": {
            "should": [
                {
                    "match": {
                        "author": author
                    },
                    "match": {
                        "content": word
                    }
                }
            ]
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        return result
    except Exception as e:
        print(f"{e}: не удалось найти книги автора {author} со словом {word}")
        return Exception

    total_books = result["hits"]["total"]["value"]
    print(f"Найдено {total_books} книг")

    for hit in result["hits"]["hits"]:
        book_title = hit["_source"]["title"]
        author = hit["_source"]["author"]
        year_pub = hit["_source"]["year_pub"]
        print(f"{book_title}, {author}, {year_pub}")


def search_dates(yfrom, ytill, word):
    """
    Функция поиска книг по заданным годам
    :return: result
    """
    query = {
        "bool": {
            "must": [
                {
                    "match": {
                        "content": word
                    }
                },
                {
                    "range": {
                        "year_pub": {
                            "gte": yfrom,
                            "lte": ytill
                        }
                    }
                }
            ]
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        return result
    except Exception as e:
        print(f"{e}: не удалось посчитать количество документов со словом {word}")
        return Exception

    total_books = result["hits"]["total"]["value"]
    print(f"Найдено {total_books} книг")

    for hit in result["hits"]["hits"]:
        book_title = hit["_source"]["title"]
        author = hit["_source"]["author"]
        year_pub = hit["_source"]["year_pub"]
        print(f"{book_title}, {author}, {year_pub}")


def calc_date(author):
    """
    Функция поиска книг по заданным годам
    :return: result
    """
    query = {
        "match": {
            "author": author
        }
    }

    aggs = {
        "avg_year": {
            "avg": {
                "field": "year_pub"
            }
        }
    }

    try:
        result = client.search(index=index_name, query=query, aggs=aggs)
        return result
    except Exception as e:
        print(f"{e}: не удалось найти книги в заданном диапозоне")
        return Exception

    avg_year = result["aggregations"]["avg_year"]["value"]
    print(f"Среднее арифметическое выпуска книг {avg_year}")


def top_words(year_pub):
    """
    Функция поиска самых часто используемых слов в тексте
    :return: None
    """
    query = {
        "match": {
            "year_pub": year_pub
        }
    }

    # aggs = {
    #     "top_words": {
    #         "terms": {
    #             "field": "content.keywords",
    #             "size": 10
    #         }
    #     }
    # }

    try:
        result = client.search(index=index_name, query=query)
        return result
    except Exception as e:
        print(f"{e}: не удалось посчитать количество часто используемых слов в тексте")
        return Exception

    words_count = Counter()
    for hit in result["hits"]["hits"]:
        content = hit["_source"]["content"]
        words = content.split()  # Разбиваем содержимое книги на слова
        words_count.update(words)  # Обновляем счетчик упоминаний слов

    # Вывод топ-10 слов с наибольшим количеством упоминаний
    print("Топ-10 самых популярных слов с количеством их упоминаний:")
    for word, count in words_count.most_common(10):
        print(f"{word}: {count}")


query = {
    "match_all": {}
}

# resp = client.search(index=index_name, query=query)

# def truncate_fb2(file_path):
#     with open(file_path, 'r', encoding='utf-8') as file:
#         content = file.read()
#
#         # Находим позицию первого вхождения фразы
#         index = content.find("<section><title><p>Том первый")
#
#         # Обрезаем файл до первой встреченной фразы
#         truncated_content = content[index:]
#
#         # Сохраняем обрезанный контент в новый файл
#         truncated_file_path = file_path.replace('.fb2', '_truncated.fb2')
#         with open(truncated_file_path, 'w', encoding='utf-8') as truncated_file:
#             truncated_file.write(truncated_content)
#
#         return truncated_file_path
#
#
#
# def extract_volume_part_and_chapter(title):
#     """
#     Извлекает том, часть и главу из заголовка.
#     """
#     volume = 0
#     part = 0
#     chapter = 0
#
#     if "Том" in title:
#         try:
#             volume = int(title.split("Том")[1].split()[0])
#         except ValueError:
#             pass
#
#     if "часть" in title.lower():
#         try:
#             part = int(title.lower().split("часть")[1].split()[0])
#         except ValueError:
#             pass
#
#     if any(c.isdigit() for c in title.split()):
#         try:
#             chapter = int(''.join(filter(str.isdigit, title.split()[-1])))
#         except ValueError:
#             pass
#
#     return volume, part, chapter

def text_fix(text):
    text = text.lower()
    numbers = {
        'первый': 1, 'первая': 1,
        'второй': 2, 'вторая': 2,
        'третий': 3, 'третья': 3,
        'четвертый': 4, 'четвертая': 4,
        'пятый': 5, 'пятая': 5,
    }
    return numbers.get(text)


def roman_to_int(s):
    roman_numerals = {
        'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100,
        'D': 500, 'M': 1000
    }

    result = 0
    prev_value = 0

    for char in reversed(s):
        value = roman_numerals[char]
        if value >= prev_value:
            result += value
        else:
            result -= value
        prev_value = value

    return result


def add_fb2(author, title, file_path):
    """
    Функция загрузки текста книги из файла
    :return: document
    """
    namespace = {'fb2': 'http://www.gribuser.ru/xml/fictionbook/2.0'}

    tree = ET.parse("/input/" + file_path)
    root = tree.getroot()

    sections = root.findall('.//fb2:section', namespace)

    tom_number = 0
    part_number = 0

    for section in sections:
        title_elem = section.find('.//fb2:title', namespace)
        if title_elem is not None:
            title_text = ''.join(title_elem.itertext()).strip()

            volume_match = re.match(r'Том\s+(.*)', title_text, re.IGNORECASE)
            if volume_match:
                tom_number = text_fix(volume_match.group(1).strip())
                continue

            part_match = re.match(r'Часть\s+(.*)', title_text, re.IGNORECASE)
            if part_match:
                part_number = text_fix(part_match.group(1).strip())
                continue

            chapter_match = re.match(r'^(I+|V?I{0,3}|[IVXLCDM]+)$', title_text, re.IGNORECASE)
            if chapter_match:
                chapter_number = roman_to_int(chapter_match.group(1).strip())
                chapter_content = ''.join(section.itertext()).strip()[2:]

                doc = {
                    'author': author,
                    'name': title,
                    'volume': tom_number,
                    'part': part_number,
                    'chapter': chapter_number,
                    'content': chapter_content,
                    'file_path': file_path,
                }
                try:
                    client.index(index=index_name, body=doc)
                    print(f"Глава {chapter_number} книги {title} автора {author} успешно загружена в Elasticsearch")
                except FileNotFoundError as e:
                    print(f"{e}: добавление не удалось")
                    return FileNotFoundError


def get_chapter(abc, limit):
    """
    Функция получения заданной главы
    :return: result
    """
    tom_number = abc.split('-')[0]
    part_number = abc.split('-')[1]
    chapter_number = abc.split('-')[2]

    print(f"Tom {tom_number}, part {part_number}, chapter {chapter_number}")

    query = {
        "bool": {
            "must": [
                {"match": {"volume": tom_number}},
                {"match": {"part": part_number}},
                {"match": {"chapter": chapter_number}}
            ]
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        print(result['hits']['hits'][0]["_source"]["content"][:int(limit)])
        return result
    except Exception as e:
        print(f"{e}: не удалось найти главу")
        return Exception


def get_text(text):
    """
    Функция поиска глав с текстом
    :return: result
    """
    query = {
        "match": {
            "content": text
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        total_books = result["hits"]["total"]["value"]
        print(f"Найдено {total_books} глав с заданным текстом")

        for hit in result["hits"]["hits"]:
            volume = hit["_source"]["volume"]
            part = hit["_source"]["part"]
            chapter = hit["_source"]["chapter"]
            print(f"{volume}-{part}-{chapter}")

        return result
    except Exception as e:
        print(f"{e}: не удалось посчитать количество документов со словом {text}")
        return Exception


def summarize_text(find):
    """
    Функция реферирования текста главы
    :return: result
    """
    if find[0].isdigit():
        tom_number = find.split('-')[0]
        part_number = find.split('-')[1]
        chapter_number = find.split('-')[2]
        query = {
            "bool": {
                "must": [
                    {"term": {"volume": tom_number}},
                    {"term": {"part": part_number}},
                    {"term": {"chapter": chapter_number}}
                ]
            }
        }
    else:
        query = {
            "match": {
                "content": find
            }
        }

    try:
        result = client.search(index=index_name, query=query)
        nlp = spacy.load("ru_core_news_sm")
        hit = result['hits']['hits']
        if not hit:
            print("Главы с фразой не нашлось")
        content = hit[0]['_source']['content']


        keywords = []
        tags = ['PROPN', 'ADJ', 'NOUN', 'VERB'] # содержит список частей речи (POS tags), которые будут использоваться для определения ключевых слов. В данном случае это существительные (NOUN), прилагательные (ADJ), глаголы (VERB) и имена собственные (PROPN).
        doc = nlp(content) # выполняет обработку текста content с помощью загруженной ранее русской модели SpaCy (ru_core_news_sm)
        for token in doc:
            if (token.text in nlp.Defaults.stop_words or token.text in punctuation):
                continue
            if (token.pos_ in tags): # .pos_ = часть речи токена
                keywords.append(token.text)

        word_freq = Counter(keywords)
        max_freq = Counter(keywords).most_common(1)[0][1]
        for w in word_freq:
            word_freq[w] = (word_freq[w] / max_freq)

        # важность каждого предложения
        sent_power = {}
        for sent in doc.sents:
            for word in sent:
                if word.text in word_freq.keys():
                    if sent in sent_power.keys():
                        sent_power[sent] += word_freq[word.text]
                    else:
                        sent_power[sent] = word_freq[word.text]

        summary = []

        sorted_x = sorted(sent_power.items(), key=lambda kv: kv[1], reverse=True) # сортируем предложения по силе

        counter = 0
        for i in range(len(sorted_x)):
            summary.append(str(sorted_x[i][0]).capitalize())

            counter += 1
            if (counter >= 3):
                break

        print(' '.join(summary))
        return result
    except Exception as e:
        print(f"{e}: не удалось найти документ со словом {find}")
        return Exception


if __name__ == '__main__':
    create()
    count_books_with_words("Длинная")
    search_books("Лев Толстой", "Длинная")
    search_dates(1000, 2000, "Длинная")
    calc_date("Толстой")
    top_words(1865)
    # print(resp)