from main import *
import sys
import argparse
import urllib3


def parse_cmd_args(cmd_args: list):
    """
    Разбор аргументов командной строки
    :param cmd_args: список аргументов командной строки
    :return: значения аргументов командной строки
    """
    parser = argparse.ArgumentParser(add_help=False)

    subparsers = parser.add_subparsers(dest='subparser_name')

    # общие параметры
    parameter_help = 'справка о работе модуля'
    parser.add_argument('-h', '--help', action='help', help=parameter_help)

    subparser_help = 'Создать индекс и mapping для Elasticsearch'
    pcreate = subparsers.add_parser('create', help=subparser_help)

    subparser_help = 'Добавить книгу из файла'
    padd_book = subparsers.add_parser('add_book', help=subparser_help, add_help=False)
    padd_book.add_argument('--author', required=True)
    padd_book.add_argument('--year', type=int, required=True)
    padd_book.add_argument('--name', required=True)
    padd_book.add_argument('file')

    subparser_help = 'Добавить книги из каталога'
    padd_books = subparsers.add_parser('add_books', help=subparser_help, add_help=False)
    padd_books.add_argument('dir')

    subparser_help = 'Поиск всех книг с заданным словом'
    pcount_books_with_words = subparsers.add_parser('count_books_with_words', help=subparser_help, add_help=False)
    pcount_books_with_words.add_argument('word')

    subparser_help = 'Поиск всех книг автора с заданным словом'
    psearch_books = subparsers.add_parser('search_books', help=subparser_help, add_help=False)
    psearch_books.add_argument('--author', required=True)
    psearch_books.add_argument('word')

    subparser_help = 'Поиск всех книг из указанного диапозона годов'
    psearch_dates = subparsers.add_parser('search_dates', help=subparser_help, add_help=False)
    psearch_dates.add_argument('--yfrom', type=int, required=True)
    psearch_dates.add_argument('--ytill', type=int, required=True)
    psearch_dates.add_argument('word')

    subparser_help = 'Среднее арифметическое для года издания заданного автора'
    pcalc_date = subparsers.add_parser('calc_date', help=subparser_help, add_help=False)
    pcalc_date.add_argument('--author', required=True)

    subparser_help = 'Топ 10 самых популярных слов книг заданного года'
    ptop_words = subparsers.add_parser('top_words', help=subparser_help, add_help=False)
    ptop_words.add_argument('--date', type=int, required=True)

    subparser_help = 'Добавить Войну и мир из файла'
    padd_book = subparsers.add_parser('add_fb2', help=subparser_help, add_help=False)
    padd_book.add_argument('-a', '--author', required=True)
    padd_book.add_argument('-n', '--name', required=True)
    padd_book.add_argument('file')

    subparser_help = 'Возвращает заданную главу часть и том'
    pget = subparsers.add_parser('get_chapter', help=subparser_help, add_help=False)
    pget.add_argument('abc')
    pget.add_argument('--limit', required=False, default=50)

    subparser_help = 'Возвращает главы с заданным текстом'
    pget_text = subparsers.add_parser('get_text', help=subparser_help, add_help=False)
    pget_text.add_argument('text')

    subparser_help = 'Реферирует текст'
    psum = subparsers.add_parser('summarize_text', help=subparser_help, add_help=False)
    psum.add_argument('-t', '--text')
    psum.add_argument('-c', '--chapter')


    return parser.parse_args(cmd_args)


if __name__ == '__main__':
    urllib3.disable_warnings()
    cmd_args = parse_cmd_args(sys.argv[1:])
    print(cmd_args)
    work_mode = cmd_args.subparser_name

    if work_mode == 'create':
        create()

    if work_mode == 'add_book':
        add_book(cmd_args.name, cmd_args.author, cmd_args.year, cmd_args.file)

    if work_mode == 'add_books':
        add_books(cmd_args.dir)

    if work_mode == 'count_books_with_words':
        count_books_with_words(cmd_args.word)

    if work_mode == 'search_books':
        search_books(cmd_args.author, cmd_args.word)

    if work_mode == 'search_dates':
        search_dates(cmd_args.yfrom, cmd_args.ytill, cmd_args.word)

    if work_mode == 'calc_date':
        calc_date(cmd_args.author)

    if work_mode == 'top_words':
        top_words(cmd_args.date)

    if work_mode == 'add_fb2':
        add_fb2(cmd_args.author, cmd_args.name, cmd_args.file)

    try:
        if work_mode == 'get_chapter':
            if int(cmd_args.limit) < 1 or int(cmd_args.limit) > 100:
                raise ValueError
            get_chapter(cmd_args.abc, cmd_args.limit)
    except ValueError as err:
        print("Введите число в диапозоне от 1 до 100")


    if work_mode == 'get_text':
        get_text(cmd_args.text)

    if work_mode == 'summarize_text':
        if cmd_args.text is None:
            summarize_text(cmd_args.chapter)
        else:
            summarize_text(cmd_args.text)