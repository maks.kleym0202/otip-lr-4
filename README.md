# Запуск докер


```bash
sudo docker build -t 2020-3-05-kle-lr4  .
```

C прокси
```bash
sudo docker build --build-arg PROXY=http://login:pass@192.168.232.1:3128 -t 2020-3-05-kle-lr1 .
```


Создание индекса:
```bash
sudo docker run --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 create

```


Добавление войны и мир:
```bash
sudo docker run -v "${PWD}"/input:/input --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 add_fb2 --author "Толстой" --name "Война и мир" "Толстой Лев. Война и мир. Том 1 и 2 - royallib.com.fb2"
```

Получить главу по номеру:
```bash
sudo docker run --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 get_chapter 1-1-2 --limit 50
```

Получить главы содержащие текст:
```bash
sudo docker run --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 get_text великолепная
```

Реферирование текста:
```bash
sudo docker run --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 summarize_text --chapter 1-1-2
sudo docker run --network=otip-lr-4_lr-3 -it 2020-3-05-kle-lr4 summarize_text --text великолепная

```


# Работа с Elastic Search

Запуск Elastic Search docker с пробросом специфического конфига

```bash
   sysctl -w vm.max_map_count=262144
   docker compose up
   docker cp otip-lr-4-es01-1://usr/share/elasticsearch/config/certs .
```
